const gulp = require("gulp");
const concat = require("gulp-concat");
const image = require("gulp-image");
const browserSync = require("browser-sync");
const sass = require("gulp-sass");
const del = require("del");
const autoprefixer = require("gulp-autoprefixer");
const rename = require("gulp-rename");
const csso = require('gulp-csso');

// Таски

gulp.task('html', function () {
    gulp.src('./src/**/*.html')
        .pipe(gulp.dest('./dist'));
});

gulp.task('fonts', function () {
    gulp.src('./src/**/fonts/**')
        .pipe(gulp.dest('./dist'));
});

gulp.task('images', function () {
    gulp.src('./src/**/images/**/*')
        .pipe(gulp.dest('./dist'));
});

gulp.task('sass', function () {
    return gulp.src('./src/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 15 versions'],
        }))
        .pipe(concat('style.css'))
        .pipe(gulp.dest('./dist/styles'))
        .pipe(csso())
        .pipe(rename('style.min.css'))
        .pipe(gulp.dest('./dist/styles'));
});

gulp.task('reloader', function () {
    browserSync({
        server:{
            baseDir:'./dist/'
        }
    });
});

gulp.task('del', function() {
    return del.sync('dist');
});

gulp.task("watch", ['del', 'sass', 'fonts', 'images', 'html', 'reloader'], function () {
    gulp.watch('./src/**/*.html', ['html']);
    gulp.watch('./src/**/fonts/**', ['fonts']);
    gulp.watch('./src/**/images/**/*', ['images']);
    gulp.watch('./dist/**/*.html', browserSync.reload);
    gulp.watch('./src/**/*.scss', ['sass']);
    gulp.watch('./dist/styles/**/*.css').on('change', browserSync.reload);
});